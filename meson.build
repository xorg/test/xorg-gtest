# Note: this project is to be used as a *source* dependency and as
# subproject in a parent project.
# see the PUBLIC API comment at the bottom for which variables may be used
#
project('xorg-gtest', 'cpp',
	version: '0.7.1',
	license: 'MIT/Expat',
	default_options: ['warning_level=2'])

pkgconfig = import('pkgconfig')
dep_x11 = dependency('x11')
dep_xi = dependency('xi')
dep_evemu = dependency('evemu')

dummy_conf = join_paths(meson.current_source_dir(), 'data', 'xorg', 'gtest', 'dummy.conf')

#
# build gtest itself as static library
#
gtest_sources = files(
	'gtest/include/gtest/gtest.h',
	'gtest/include/gtest/gtest-spi.h',
	'gtest/src/gtest-all.cc',
)
gtest_includes = [
	include_directories('gtest/include'),
	include_directories('gtest/'),
]
gtest_deps = [
	dependency('threads'),
]
libgtest = static_library('gtest',
			  gtest_sources,
			  dependencies: gtest_deps,
			  include_directories: gtest_includes)
dep_gtest = declare_dependency(link_with: libgtest)

xorg_gtest_sources = files(
	'include/xorg/gtest/xorg-gtest-environment.h',
	'include/xorg/gtest/xorg-gtest-process.h',
	'include/xorg/gtest/xorg-gtest-test.h',
	'include/xorg/gtest/xorg-gtest-xserver.h',
	'include/xorg/gtest/evemu/xorg-gtest-device.h',
	'include/xorg/gtest/xorg-gtest.h',

	'src/defines.h',
	'src/xorg-gtest_main.cpp',
	'src/environment.cpp',
	'src/device.cpp',
	'src/process.cpp',
	'src/test.cpp',
	'src/xserver.cpp',
	'src/xorg-gtest-all.cpp',
)

xorg_gtest_incs = [
	gtest_includes,
	include_directories('include'),
]
xorg_gtest_dependencies = [
	dep_gtest,
	dep_evemu,
	dep_x11,
	dep_xi,
]
lib_xorg_gtest = static_library('xorg-gtest',
				xorg_gtest_sources,
				include_directories: xorg_gtest_incs,
				dependencies: xorg_gtest_dependencies,
				cpp_args: [
					'-DDUMMY_CONF_PATH="@0@"'.format(dummy_conf),
					'-DLOGFILE_DIR="@0@"'.format(get_option('logpath')),
				],
				)

dep_xorg_gtest = declare_dependency(link_with: lib_xorg_gtest,
				    include_directories: [gtest_includes, xorg_gtest_incs])


#
# tests
#

recordings = [
	'test/SynPS2-Synaptics-TouchPad.desc',
	'test/PIXART-USB-OPTICAL-MOUSE.desc'
]
foreach r: recordings
	configure_file(input: r, output: '@PLAINNAME@', copy: true, install: false)
endforeach

executable('process-test-helper',
	   'test/process-test-helper.cpp',
	   install: false)
test('test-process',
	executable('test-process',
		   'test/process-test.cpp',
		   dependencies: dep_xorg_gtest,
		   cpp_args: [
			   '-DTEST_ROOT_DIR="@0@/test/"'.format(meson.current_source_dir()),
			   '-DTEST_BUILD_DIR="@0@/"'.format(meson.current_build_dir()),
		   ],
		   install: false,
	),
	is_parallel: false,
)
test('test-device',
	executable('test-device',
		   'test/device-test.cpp',
		   dependencies: dep_xorg_gtest,
		   cpp_args: [
			   '-DTEST_ROOT_DIR="@0@/test/"'.format(meson.current_source_dir()),
		   ],
		   install: false,
	),
	is_parallel: false,
)
executable('xserver-test-helper',
	   'test/xserver-test-helper.cpp',
	   install: false)
test('test-xserver',
	executable('test-xserver',
		   'test/xserver-test.cpp',
		   dependencies: dep_xorg_gtest,
		   cpp_args: [
			   '-DTEST_ROOT_DIR="@0@/test/"'.format(meson.current_source_dir()),
			   '-DTEST_BUILD_DIR="@0@/"'.format(meson.current_build_dir()),
			   '-DDUMMY_CONF_PATH="@0@"'.format(dummy_conf),
			   '-DLOGFILE_DIR="@0@"'.format(get_option('logpath')),
		   ],
		   install: false,
	),
	is_parallel: false,
)

#
# PUBLIC API:
# The variables below are the ones that may be used by any parent
# projects.
#
# Use with:
# xorg_gest = subproject('xorg-gtest')
# executable('foo',
#            dependencies: xorg_gtest.get_variable('xorg_gtest_dependency'),
#            include_directories: xorg_gtest.get_variable('xorg_gtest_includes'))
#
xorg_gtest_dependency = dep_xorg_gtest
xorg_gtest_includes = xorg_gtest_incs
